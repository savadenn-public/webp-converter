<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use App\Services\ConverterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use WebPConvert\Exceptions\WebPConvertException;

/**
 * Class ConvertController
 */
class ConvertController extends AbstractController
{
    /**
     * Absolute path of the public directory
     *
     * @var string
     */
    private string $publicDir;

    /**
     * ConvertController constructor.
     *
     * @param string $publicDir
     */
    public function __construct(string $publicDir)
    {
        $this->publicDir = $publicDir;
    }

    /**
     * Converts the uploaded image into WEBP
     *
     * @Route("/convert", name="app.convert", methods={"POST"})
     *
     * @param Request            $request
     * @param ConverterInterface $converter
     *
     * @return JsonResponse
     */
    public function index(Request $request, ConverterInterface $converter): JsonResponse
    {
        $image = new Image();
        $form  = $this->createForm(ImageType::class, $image);
        $form->submit(['file' => $request->files->get('image')]);

        if (!$form->isValid()) {
            return new JsonResponse(['message' => (string) $form->getErrors(true)], JsonResponse::HTTP_BAD_REQUEST);
        }

        $input  = $image->getFile();
        try {
            $output = $converter->convertFile($input);
        } catch (WebPConvertException $exception) {
            return new JsonResponse(['message' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
        $result = [
            "input" => [
                "size" => $input->getSize(),
                "type" => $input->getMimeType(),
            ],
            "output" => [
                "size"   => $output->getSize(),
                "type"   => "image/webp",
                "ratio"  => $output->getSize() / $input->getSize(),
                "url"    => $this->getRelativePath($output),
            ],
        ];

        return new JsonResponse($result);
    }

    /**
     * Return the path of $file relative to /public dir
     *
     * @param File $file
     *
     * @return string
     */
    private function getRelativePath(File $file): string
    {
        return str_replace($this->publicDir, '', $file->getPathname());
    }
}
