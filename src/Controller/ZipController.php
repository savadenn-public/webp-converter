<?php

namespace App\Controller;

use App\Validator\Constraints\IsPublic;
use App\Zipper\ZipperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ZipController
 */
class ZipController extends AbstractController
{

    /**
     * Generate a ZIP archive with given files & return it as a BinaryFileResponse
     *
     * @Route("/zip", name="app.zip", methods={"POST"})
     *
     * @param Request            $request
     * @param ZipperInterface    $zipper
     * @param ValidatorInterface $validator
     *
     * @return BinaryFileResponse
     */
    public function index(Request $request, ZipperInterface $zipper, ValidatorInterface $validator): BinaryFileResponse
    {
        $files = $request->get('files', ['']);

        // Apply same validation constraints on all files
        $constraint = new All([
            new Type("string"),
            new NotBlank(),
            new IsPublic(),
        ]);
        $violations = $validator->validate($files, $constraint);

        if ($violations->count() > 0) {
            // TODO format error message
            throw new BadRequestException($violations->get(0)->getMessage());
        }

        return new BinaryFileResponse($zipper->zip($files));
    }
}
