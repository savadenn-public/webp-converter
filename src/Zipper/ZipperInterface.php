<?php

namespace App\Zipper;

/**
 * Interface ZipperInterface
 */
interface ZipperInterface
{
    /**
     * Add given files into a simple ZIP archive
     *
     * @param string[] $files A list of file path to zip
     *
     * @return string
     */
    public function zip(array $files): string;
}
