<?php

namespace App\Zipper;

/**
 * Class Zipper
 */
class Zipper implements ZipperInterface
{
    /**
     * Absolute path of directory where to generate the archive
     *
     * @var string
     */
    private string $zipDir;

    /**
     * Absolute path of the public directory
     *
     * @var string
     */
    private string $publicDir;

    /**
     * A secret
     *
     * @var string
     */
    private string $secret;

    /**
     * ZipController constructor.
     *
     * @param string $zipDir    Absolute path of directory where to generate the archive
     * @param string $publicDir Absolute path of the public directory
     * @param string $secret    Application secret
     */
    public function __construct(string $zipDir, string $publicDir, string $secret)
    {
        $this->zipDir    = $zipDir;
        $this->secret    = $secret;
        $this->publicDir = $publicDir;
    }

    /**
     * Zip given files & return the absolute path of the archive
     * /!\ Make sure to validate $files to prevent path traversal attacks
     *
     * @param string[] $files A list of file paths relative to /public dir
     *
     * @return string
     */
    public function zip(array $files): string
    {
        $archive  = new \ZipArchive();
        $filename = $this->generateRandomName();

        if (true !== ($status = $archive->open($filename, \ZipArchive::CREATE))) {
            throw new ZipperException('Cannot create archive.', $status);
        }

        foreach ($files as $file) {
            $filePath = $this->publicDir.$file;

            if (!$archive->addFile($filePath, basename($file))) {
                throw new ZipperException(sprintf('Cannot add file "%s" to archive.', $filePath));
            }
        }

        $archive->close();

        return $filename;
    }

    /**
     * Generate a random file name & return its absolute path
     *
     * @return string
     */
    private function generateRandomName(): string
    {
        return $this->zipDir.'/'.sha1(rand(0, 100000).time().$this->secret).'.zip';
    }
}
