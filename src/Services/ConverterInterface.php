<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface ConverterInterface
 */
interface ConverterInterface
{
    /**
     * Look recursively for images in $source and convert them into WEBP
     *
     * @param string      $source      An image file or a directory containing images
     * @param string|null $destination The destination directory to convert images
     */
    public function convert(string $source, ?string $destination): void;

    /**
     * Convert the given image into WEBP and return a File object representing the converted image
     *
     * @param UploadedFile $file The image to convert
     *
     * @return File
     */
    public function convertFile(UploadedFile $file): File;
}
