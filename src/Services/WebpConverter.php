<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WebPConvert\WebPConvert;

/**
 * Class WebpConverter
 */
class WebpConverter implements ConverterInterface
{
    const IMG_PATTERN = '#\.(jpe?g|png|bmp)$#i';

    /**
     * @var string
     */
    private string $outputDir;

    /**
     * @var string
     */
    private string $secret;

    /**
     * WebpConverter constructor.
     *
     * @param string $outputDir The directory where WEBP are generated
     * @param string $secret    A secret
     */
    public function __construct(string $outputDir, string $secret)
    {
        $this->outputDir = $outputDir;
        $this->secret    = $secret;
    }

    /**
     * @inheritDoc
     */
    public function convert(string $source, ?string $destination): void
    {
        if ($this->isImage($source)) {
            if (null === $destination) {
                $destination = preg_replace(self::IMG_PATTERN, '.webp', $source);
            } else {
                $destination .= '/'.preg_replace(self::IMG_PATTERN, '.webp', basename($source));
            }

            WebPConvert::convert($source, $destination);
        } elseif (is_dir($source)) {
            foreach (scandir($source) as $file) {
                if ('.' === $file || '..' === $file) {
                    continue;
                }

                $this->convert($source.'/'.$file, $destination);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function convertFile(UploadedFile $file): File
    {
        // Generate a unique directory using app secret & file hash
        $dirname = $this->generateUniqueDirName($file->getPathname());
        // Get a filename with .webp extension
        $filename = $this->changeExtension($file->getClientOriginalName());
        // Full path of the output file
        $output = $this->outputDir.DIRECTORY_SEPARATOR.$dirname.DIRECTORY_SEPARATOR.$filename;

        // Don't regenerate file if exists already
        if (!file_exists($output)) {
            WebPConvert::convert($file->getPathname(), $output);
        }

        return new File($output);
    }

    /**
     * Generate a unique directory based on file hash & app secret
     *
     * @param string $filepath
     *
     * @return string
     */
    public function generateUniqueDirName(string $filepath): string
    {
        return hash('sha256', hash_file('sha256', $filepath).$this->secret);
    }

    /**
     * Return whether the given source is an image file
     *
     * @param string $source
     *
     * @return bool
     */
    private function isImage(string $source): bool
    {
        return is_file($source) && preg_match(self::IMG_PATTERN, $source);
    }

    /**
     * Replace extension of $filename with the given $extension (default "webp")
     *
     * @param string $filename
     * @param string $extension
     *
     * @return string
     */
    private function changeExtension(string $filename, string $extension = 'webp'): string
    {
        return preg_replace(self::IMG_PATTERN, ".$extension", $filename);
    }
}
