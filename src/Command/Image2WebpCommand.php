<?php declare(strict_types=1);
namespace App\Command;

use App\Services\ConverterInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Image2WebpCommand.
 */
class Image2WebpCommand extends Command
{

    /**
     * @var string
     */
    protected static $defaultName = 'app:img2webp';

    /**
     * @var ConverterInterface
     */
    private ConverterInterface $converter;

    /**
     * Image2WebpCommand constructor.
     *
     * @param ConverterInterface $converter
     */
    public function __construct(ConverterInterface $converter)
    {
        $this->converter = $converter;
        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->addArgument('source', InputArgument::REQUIRED, 'The source directory')
             ->addArgument('destination', InputArgument::OPTIONAL, 'The destination directory. If not set, images will be generated just in place');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $source      = $input->getArgument('source');
        $destination = $input->getArgument('destination');

        if (!is_dir($source)) {
            throw new InvalidArgumentException("\"$source\" is not a valid source directory");
        }

        $this->converter->convert($source, $destination);

        return 0;
    }
}
