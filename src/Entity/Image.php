<?php

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An uploaded image
 */
class Image
{

    /**
     * @Assert\Image(maxSize = "5Mi", detectCorrupted=true)
     * @Assert\NotBlank()
     *
     * @var UploadedFile|null
     */
    private ?UploadedFile $file;

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     *
     * @return self
     */
    public function setFile(UploadedFile $file): self
    {
        $this->file = $file;

        return $this;
    }
}
