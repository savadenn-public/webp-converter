<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * This validator asserts a path is publicly accessible
 */
class IsPublicValidator extends ConstraintValidator
{
    /**
     * Absolute path of the directory used as base for validation.
     * Must be a sub path of public dir.
     * All valid files must be under this directory.
     *
     * @var string
     */
    private string $baseDir;

    /**
     * Absolute path of the public directory.
     * As long as validated value is relative path to public dir, it's used to re-build the absolute path.
     *
     * @var string
     */
    private string $publicDir;

    /**
     * FooValidator constructor.
     *
     * @param string $publicDir
     * @param string $baseDir
     */
    public function __construct(string $publicDir, string $baseDir)
    {
        if (!is_dir($publicDir)) {
            throw new \InvalidArgumentException(sprintf('Parameter $publicDir must be a valid directory path. "%s" given', $publicDir));
        }
        if (!is_dir($baseDir)) {
            throw new \InvalidArgumentException(sprintf('Parameter $baseDir must be a valid directory path. "%s" given', $baseDir));
        }
        if (!$this->isSubPathOf($publicDir, $baseDir)) {
            throw new \InvalidArgumentException('Directory $baseDir must be a sub path of $publicDir');
        }
        $this->baseDir   = $baseDir;
        $this->publicDir = $publicDir;
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof IsPublic) {
            throw new UnexpectedTypeException($constraint, IsPublic::class);
        }

        if (!is_string($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedValueException($value, 'string');
        }

        $value = strval($value);

        if (!$this->isPublicFile($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }

    /**
     * Return whether $subPath is a sub path of $path
     *
     * @param string $path
     * @param string $subPath
     *
     * @return bool
     */
    private function isSubPathOf(string $path, string $subPath): bool
    {
        return false !== ($result = explode($path, $subPath))
            && isset($result[1]);
    }


    /**
     * Return whether $file is a valid file under $this->baseDir directory
     *
     * @param string $file A path relative to $this->baseDir directory
     *
     * @return bool
     */
    private function isPublicFile(string $file): bool
    {
        $fullPath = realpath($this->publicDir.$file);

        return false !== $fullPath && $this->isSubPathOf($this->baseDir, $fullPath);
    }
}
