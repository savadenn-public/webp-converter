<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * This constraint asserts the file is publicly accessible
 *
 * @Annotation
 *
 * @Target({"PROPERTY"})
 */
class IsPublic extends Constraint
{
    public string $message = 'The file "{{ string }}" does not exists or is not public.';
}
