<?php declare(strict_types=1);
namespace App\Tests\Command;

use App\Command\Image2WebpCommand;
use App\Services\WebpConverter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class Image2WebpCommandTest.
 */
class Image2WebpCommandTest extends TestCase
{
    /**
     * Test command with required arguments missing
     */
    public function testExecuteMissingArgument()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "source")');

        $tester = new CommandTester(new Image2WebpCommand(new WebpConverter('/output', '')));
        $tester->execute([]);
    }

    /**
     * Test command with invalid argument
     */
    public function testExecuteInvalidSource()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('"foo" is not a valid source directory');

        $tester = new CommandTester(new Image2WebpCommand(new WebpConverter('/output', '')));
        $tester->execute(['source' => 'foo']);
    }
}
