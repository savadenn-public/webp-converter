<?php

namespace App\Tests\Validator\Constraints;

use App\Validator\Constraints\IsPublic;
use App\Validator\Constraints\IsPublicValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

/**
 * Class IsPublicValidatorTest
 */
class IsPublicValidatorTest extends TestCase
{
    /**
     * Test constructor with non existing path $publicDir expecting exception
     */
    public function testConstructInvalidPublicDir(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Parameter $publicDir must be a valid directory path. "foo" given');

        new IsPublicValidator('foo', 'bar');
    }

    /**
     * Test constructor with a non existing path $baseDir expecting exception
     */
    public function testConstructInvalidSubDir(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Parameter $baseDir must be a valid directory path. "bar" given');

        new IsPublicValidator(__DIR__, 'bar');
    }

    /**
     * Test constructor with a path being not a sub path of $publicDir
     */
    public function testConstructBadSubDir(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Directory $baseDir must be a sub path of $publicDir');

        new IsPublicValidator(__DIR__, realpath(__DIR__.'/..'));
    }

    /**
     * Test validation with bad constraint object passed in expectiong exception
     */
    public function testValidateBadConstraint(): void
    {
        $this->expectException(UnexpectedTypeException::class);

        $validator = new IsPublicValidator(realpath(__DIR__.'/..'), __DIR__);
        $validator->validate('foo', new NotBlank());
    }

    /**
     * Test validation with a non string value expecting exception
     */
    public function testValidateBadUsage(): void
    {
        $this->expectException(UnexpectedTypeException::class);

        $validator = new IsPublicValidator(realpath(__DIR__.'/..'), __DIR__);
        $validator->validate(new \stdClass(), new IsPublic());
    }

    /**
     * Test validation exception success
     */
    public function testValidateSuccess(): void
    {
        $context = $this->getMockBuilder(ExecutionContextInterface::class)->getMock();
        $context->expects($this->never())
            ->method('buildViolation');

        $validator = new IsPublicValidator(__DIR__, __DIR__);
        $validator->initialize($context);
        $validator->validate('/'.basename(__FILE__), new IsPublic());
    }

    /**
     * Test validation expecting violation
     */
    public function testValidateFailure(): void
    {
        $violationBuilder = $this->getMockBuilder(ConstraintViolationBuilderInterface::class)->getMock();
        $violationBuilder->expects($this->once())
            ->method('setParameter')
            ->with('{{ string }}')
            ->willReturn($violationBuilder);

        $violationBuilder->expects($this->once())
            ->method('addViolation');

        $context = $this->getMockBuilder(ExecutionContextInterface::class)->getMock();
        $context->expects($this->once())
            ->method('buildViolation')
            ->with('The file "{{ string }}" does not exists or is not public.')
            ->willReturn($violationBuilder);

        $validator = new IsPublicValidator(__DIR__, __DIR__);
        $validator->initialize($context);
        $validator->validate('/../../bootstrap.php', new IsPublic());
    }
}
