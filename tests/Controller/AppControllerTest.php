<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AppControllerTest
 *
 * @group functional
 */
class AppControllerTest extends WebTestCase
{
    /**
     * Smoke test of the home page
     */
    public function testIndex(): void
    {
        $client = static::createClient();

        $client->request('GET', '');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
