export const ByteConverter = {
    /**
     * @var {Array}
     */
    sizes : ['KB', 'MB', 'GB', 'TB'],

    /**
     * Converter the given size (in bytes) into a string like 54,5MB
     *
     * @param {number} size
     * @return {String}
     */
    convert: size => {
        let result = size
        let i      = 0

        while (result > 1000) {
            result /= 1024
            i++
        }

        return ByteConverter.roundDecimal(result, 1) + '&nbsp;' +ByteConverter.sizes[i]
    },

    /**
     * Round given number applying floating precision
     *
     * @param {number} number
     * @param {number} precision
     * @return {number}
     */
    roundDecimal: (number, precision) => {
        precision = precision || 2
        let tmp = Math.pow(10, precision)

        return Math.round(number * tmp) / tmp
    }
}