import { saveAs } from 'file-saver'
import toast from 'toast-me'

export const ZipAll = {

    create: () => {
        let files = []

        document.querySelectorAll('.File--converted .File-path')
            .forEach(item => {
                files.push(item.getAttribute('href'))
                // saveAs(item.getAttribute('href'), 'foo.wepb')
            })

        files = files.filter((value, index, self) => self.indexOf(value) === index)
        const form = new FormData()

        for (let i=0; i<files.length; i++) {
            form.append('files[]', files[i])
        }

        fetch('/zip', {
            body: form,
            method: 'POST',
        }).then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.blob()
        }).then(blob => saveAs(blob, 'webp.zip'))
            .catch(exception => toast('something went wrong: ' + exception.toString(), 'error'))
    }
}