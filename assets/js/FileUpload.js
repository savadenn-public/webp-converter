import axios           from 'axios'
import {ByteConverter} from './ByteConverter'

export default class FileUpload {
    constructor() {
        const dropZone    = document.getElementById('drop-zone')
        const input       = document.querySelector('.InputFile')
        const fileWrapper = document.querySelector('.Files')

        input.addEventListener('change', event => {
            this.convertImagesToWebp(event.target.files, fileWrapper)
        })

        dropZone.addEventListener('click', event => input.click())

        dropZone.addEventListener('dragover', event => {
            event.stopPropagation()
            event.preventDefault()
            event.dataTransfer.dropEffect = 'copy'
        })

        dropZone.addEventListener('drop', event => {
            event.stopPropagation()
            event.preventDefault()
            const files = event.dataTransfer.files

            this.convertImagesToWebp(files, fileWrapper)
        })
    }

    /**
     *
     * @param {FileList}         images      The images to convert into webp
     * @param {HTMLUListElement} fileWrapper The wrapper where converted files are listed
     */
    convertImagesToWebp(images, fileWrapper) {
        document.querySelector('.FilesWrapper').classList.remove('FilesWrapper--hidden')
        const promises = []

        images.forEach(image => {
            const element = this.createFileElement(image, fileWrapper)

            promises.push(
                this.convertImageToWebp(image, element)
                    .then(data   => this.displayConvertedFile(element, data.output))
                    .catch(error => this.displayFailedFile(element, error))
            )
        })

        Promise.all(promises).then(this.enableDownloadButton)
    }

    /**
     * Convert the given image into Webp
     *
     * @param {File}          image   The image to convert
     * @param {HTMLLIElement} element The <li> element representing the file in the queue
     * @returns {Promise<object>}
     */
    async convertImageToWebp(image, element) {
        const formData = new FormData()
        formData.append('image', image)

        // TODO: use an URL generator
        const response = await axios.post('/convert', formData, {
            headers: {'Content-Type': 'multipart/form-data'},
            onUploadProgress: event => this.updateProgressBar(element.querySelector('.File-progress'), event)
        })

        return response.data
    }

    /**
     * Update progress bar during file upload
     *
     * @param {HTMLProgressElement} progressBar
     * @param {ProgressEvent}       event
     */
    updateProgressBar(progressBar, event) {
        // Update progress bar status
        const percent = Math.round(100 * event.loaded / event.total)

        // Limit percentage to 80%, the remaining 20% are for conversion process
        progressBar.value = Math.min(percent, 80)
    }

    /**
     * Create an <HTMLLIElement> in the list representing the given file to convert
     *
     * @param {File}             file
     * @param {HTMLUListElement} wrapper
     * @return {HTMLLIElement}
     */
    createFileElement(file, wrapper) {
        const item = document.querySelector('.FileTemplate').content.cloneNode(true)
        item.querySelector('.File-name').innerHTML = file.name
        item.querySelector('.File-size').innerHTML = ByteConverter.convert(file.size / 1024)

        // As item is a DocumentFragment, its entire contents are moved into the child list of the specified parent node
        wrapper.appendChild(item)

        // Get inserted element
        return wrapper.querySelector('.File:last-child')
    }

    enableDownloadButton() {
        if (document.querySelectorAll('.File--converted').length) {
            document.querySelector('.Button--download').classList.remove('Button--hidden')
        }
    }

    /**
     * Mark the element as successful & update link to download converted file
     *
     * @param {Element} element
     * @param {object}  webpImage The converted image received from server
     */
    displayConvertedFile(element, webpImage) {
        element.classList.add('File--converted')

        element.querySelector('.File-finalSize').innerHTML   = ByteConverter.convert(webpImage.size / 1024)
        element.querySelector('.File-progress').value        = 100
        element.querySelector('.File-progress').innerHTML    = 'Finished'
        element.querySelector('.File-compression').innerHTML = -Math.ceil(100 * (1 - webpImage.ratio)) + '%'
        element.querySelector('.File-path').setAttribute('href', webpImage.url)
}

    /**
     * Mark the given element as failed
     *
     * @param {HTMLLIElement} element
     * @param {Error}         error
     */
    displayFailedFile(element, error) {
        const defaultMessage = 'An unexpected error occurred.'

        element.classList.add('File--failed')
        element.querySelector('.File-message').innerHTML = error.response.data.message ?? defaultMessage
        element.querySelector('.File-progress').value = 0
    }
}
