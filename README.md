# Installation
1. Install dependencies
```bash
composer install
yarn install
```

2. Build assets
```bash
yarn encore dev --watch
```

3. Run
```bash
php -S 127.0.0.1:8000 -t public
```
Or if you have Symfony CLI:
```bash
symfony server:start
```
