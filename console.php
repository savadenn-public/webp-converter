#!/usr/bin/php
<?php

require_once __DIR__.'/vendor/autoload.php';

use App\Command\Image2WebpCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new Image2WebpCommand());
$application->run();
